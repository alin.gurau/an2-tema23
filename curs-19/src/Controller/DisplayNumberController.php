<?php
namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
// use Symfony\Flex\Response;

class DisplayNumberController extends AbstractController
{
    /**
     *@Route("/number/display/{number1},{number2}", name="my-number")
     */
    public function displayNumber($number1, $number2)
    {

        $url=$this->generateUrl('my-number', [ 'number1'=>12, 'number2'=>21]);
        $sum = $number1+$number2;
        return $this->render('number.html.twig', ['number1'=>$number1, 'number2'=>$number2, 'sum'=>$sum, 'url'=>$url]);
        // return new Response(
        //     '<html><body> Sum of numbers: '. $number1 . '+'. $number2.'='   .$sum .'</body></html>'
        // );
    }

    /**
     * @Route("/home", name="homepage")
     */
    public function homepage(){
        return $this->redirectToRoute('my-number',['number1'=>2,'number2'=>5]);
        // return new Response(
        //     '<html><body> This the HomePage </body></html>'
        // );
    }
}
 