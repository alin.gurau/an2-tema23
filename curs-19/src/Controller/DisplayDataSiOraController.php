<?php
namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;


class DisplayDataSiOraController extends AbstractController
{
    /**
     * @Route("/datasiora", name="display-date")
     */
    public function displayDataSiOra(){
        date_default_timezone_set('Europe/Bucharest');
        $date = date('d-m-Y H:i:s');
        return $this->render('time.html.twig',['date'=>$date]);
    }

}
?>