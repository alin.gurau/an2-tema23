<?php
namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;


class CompareRandomNumberController extends AbstractController{
    
    /**
     * @Route("/compareNumber", name="compare-number")
     */
    public function compareRandomNumber(){

        $url=$this->generateUrl('compare-number');

        $randomNumber   = rand(0, 100);
        $insertedNumber = '';

        if (isset($_POST['submit'])) {
            $insertedNumber = $_POST[ 'insertedNumber'];
            if ($insertedNumber > $randomNumber) {
                    echo " Numarul introdus este PREA MARE! ";
                } elseif ( $insertedNumber < $randomNumber) {
                    echo " Numarul introdus este PREA MIC!";
                } elseif ( $insertedNumber == $randomNumber) {
                    echo " Numarul introdus este EXACT";
                } 
        }
        return $this->render('random-number.html.twig',['url'=>$url]);
    }

    public function resetPage(){
        return $this->redirectToRoute('compare-number');
    }
}
?>